//
//Copyright 2014 Micah Thomas
//
//Licensed under the Apache License, Version 2.0 (the "License");
//you may not use this file except in compliance with the License.
//You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//Unless required by applicable law or agreed to in writing, software
//distributed under the License is distributed on an "AS IS" BASIS,
//WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//See the License for the specific language governing permissions and
//limitations under the License.


//=========================//
//===== DIGITAL WRITE =====//
//=========================//
Blockly.Blocks['digital_write'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
        .appendField("Digital Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "PIN_0"], ["1", "PIN_1"], ["2", "PIN_2"], ["3", "PIN_3"], ["4", "PIN_4"], ["5", "PIN_5"], ["6", "PIN_6"], ["7", "PIN_7"]]), "PIN");
    this.appendDummyInput()
        .appendField("write");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["High", "HIGH"], ["Low", "LOW"]]), "DO");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
Blockly.Python['digital_write'] = function(block) {
  var dropdown_do = block.getFieldValue('DO');
  var dropdown_pin = block.getFieldValue('PIN');
  // TODO: Assemble Python into code variable.
  var code = 'digital_' + dropdown_pin + '.write("' + dropdown_do + '")';
  return code;
};
//=========================//
//===== DIGITAL READ ======//
//=========================//
Blockly.Blocks['digital_read'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(330);
    this.appendDummyInput()
        .appendField("Read Digital Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "PIN_0"], ["1", "PIN_1"], ["2", "PIN_2"], ["3", "PIN_3"], ["4", "PIN_4"], ["5", "PIN_5"], ["6", "PIN_6"], ["7", "PIN_7"]]), "PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
};
Blockly.Python['digital_read'] = function(block) {
  var dropdown_pin = block.getFieldValue('PIN');
  // TODO: Assemble Python into code variable.
  var code = 'digital_' + dropdown_pin + '.read()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};
//=========================//
//===== ANALOG WRITE ======//
//=========================//
Blockly.Blocks['analog_write'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(0);
    this.appendDummyInput()
        .appendField("Analog Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "PIN_0"], ["1", "PIN_1"], ["2", "PIN_2"], ["3", "PIN_3"], ["4", "PIN_4"], ["5", "PIN_5"], ["6", "PIN_6"], ["7", "PIN_7"]]), "PIN");
    this.appendDummyInput()
        .appendField("write");
    this.appendValueInput("AO")
        .setCheck("Number");
    this.appendDummyInput()
        .appendField("%");
    this.setInputsInline(true);
    this.setPreviousStatement(true);
    this.setNextStatement(true);
    this.setTooltip('');
  }
};
Blockly.Python['analog_write'] = function(block) {
  var ao = Blockly.Python.valueToCode(block, 'AO', Blockly.Python.ORDER_ATOMIC);
  var pin = block.getFieldValue('PIN');
  // TODO: Assemble Python into code variable.
  var code = 'analog_' + pin + '.write(' + ao + ')';
  return code;
};
//=========================//
//===== ANALOG READ =======//
//=========================//
Blockly.Blocks['analog_read'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(0);
    this.appendDummyInput()
        .appendField("Read Analog Pin");
    this.appendDummyInput()
        .appendField(new Blockly.FieldDropdown([["0", "PIN_0"], ["1", "PIN_1"], ["2", "PIN_2"], ["3", "PIN_3"], ["4", "PIN_4"], ["5", "PIN_5"], ["6", "PIN_6"], ["7", "PIN_7"]]), "PIN");
    this.setInputsInline(true);
    this.setOutput(true);
    this.setTooltip('');
  }
};
Blockly.Python['analog_read'] = function(block) {
  var dropdown_pin = block.getFieldValue('PIN');
  // TODO: Assemble Python into code variable.
  var code = 'analog_' + dropdown_pin + '.read()';
  // TODO: Change ORDER_NONE to the correct strength.
  return [code, Blockly.Python.ORDER_NONE];
};
//=========================//
//=====  INTERRUPT ========//
//=========================//
Blockly.Blocks['interrupt'] = {
  init: function() {
    this.setHelpUrl('http://www.example.com/');
    this.setColour(65);
    this.appendDummyInput()
        .appendField("If Interrupt");
    this.appendValueInput("TEST")
        .setCheck("String");
    this.appendStatementInput("NAME");
    this.setTooltip('');
  }
};
Blockly.Python['interrupt'] = function(block) {
  var value_test = Blockly.Python.valueToCode(block, 'TEST', Blockly.Python.ORDER_ATOMIC);
  var statements_name = Blockly.Python.statementToCode(block, 'NAME');
  // TODO: Assemble Python into code variable.
  var code = '... interrupt code ...!';
  return code;
};
//=========================//
//======== ONLOAD =========//
//=========================//
onload = function() {
    Blockly.inject(document.body,{path: '', toolbox: document.getElementById('toolbox')});
    window.parent.Blockly = Blockly;

    // Javascript to Pyside Interactions
    function updateCode() {
        var code = Blockly.Python.workspaceToCode();
        if(typeof editor != "undefined") {
            // inside pyside
            editor.setCode(code);
        }
        else {
            // inside browser
            console.log(code);
        }
    }

    window.saveToFile = function () {
        var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
        var xml_text = Blockly.Xml.domToText(xml);
        editor.consoleLog('WRITING: '+xml_text);
        return xml_text;
    }

    window.openFromFile = function (xml_text) {
        editor.consoleLog('READING: '+xml_text);
        var xml = Blockly.Xml.textToDom(xml_text);
        Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
    }
    Blockly.addChangeListener(updateCode);
};

