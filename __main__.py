# Copyright 2014 Micah Thomas
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import sys

from PySide import QtGui

from editor import Ui_MainWindow, Com
from server import ServerThread


class MyMainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MyMainWindow, self).__init__(parent)
        self.port = server.port
        self.ui = Ui_MainWindow()
        self.ui.c = Com()
        self.ui.c.closeApp.connect(self.close)
        self.ui.setupUi(self)
        self.ui.setupEditor()
        self.ui.setupJS()
        self.ui.loadUSB()

app = QtGui.QApplication(sys.argv)
server = ServerThread()
print "Server Starting UP"
server.start()
window = MyMainWindow()
print "GUI Starting UP"
window.show()
exit_1 = app.exec_()
print "GUI Closed"
print "Server Shutting DOWN"
server.exit()
sys.exit(exit_1)