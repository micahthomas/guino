# Copyright 2014 Micah Thomas
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import BaseHTTPServer
from SimpleHTTPServer import SimpleHTTPRequestHandler
from PySide import QtCore


# Silent Handler
class MyHandler(SimpleHTTPRequestHandler):
    def log_message(self, format, *args):
        return


class ServerThread(QtCore.QThread):
    def __init__(self, parent=None):
        QtCore.QThread.__init__(self, parent)
        self.port = 8000
        self.host = '127.0.0.1'
        self.handler = MyHandler
        for i in range(100):
            try:
                self.httpd = BaseHTTPServer.HTTPServer((self.host, self.port), self.handler)
                break
            except Exception as e:
                self.port += 1
                print "Something wrong with %s:%d Exception type is %s" % (self.host, self.port, e)

    def run(self):
        self.httpd.serve_forever()