#Guino

A Simple GUI Editor for rapid hardware prototyping  

###Requirements
  - Python ([2.7])
  - PySide ([>=1.2.1])

###Tech

Guino uses a number of open source projects to work properly:

* [Qt] - Qt is a cross-platform application and UI framework for developers using C++ 
* [Pyside] - The PySide project provides LGPL-licensed Python bindings for Qt
* [Blockly] - Blockly is a web-based, graphical programming editor

###Installation:

```sh
git clone https://username@bitbucket.org/micahthomas/guino.git guino
cd guino
```
###Using:
```sh
python .
```


###License
**Free Software, Hell Yeah!**

Copyright 2014 Micah Thomas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[Qt]:http://qt-project.org/
[PySide]:http://pyside.org/
[Blockly]:https://code.google.com/p/blockly/
[>=1.2.1]:http://qt-project.org/wiki/Get-PySide
[2.7]:http://www.python.org/download/
