# Copyright 2014 Micah Thomas
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from PySide import QtCore, QtGui, QtWebKit


class Com(QtCore.QObject):
    closeApp = QtCore.Signal()


class Editor(QtCore.QObject):
    def __init__(self, parent=None):
        super(Editor, self).__init__(parent)
        self.code = ""

    @QtCore.Slot(str)
    def consoleLog(self, msg):
        print msg

    @QtCore.Slot(str)
    def setCode(self, code):
        self.code = code
        print "CODE: \n" + code


class Ui_MainWindow(object):
    def loadUSB(self):
        self.comboBox.clear()
        self.comboBox.addItem("USB 1")
        self.comboBox.addItem("USB 2")
        self.comboBox.addItem("USB 3")

    def openFileDialog(self):
        #TODO: Need to find out what is causing errors with FileDialog
        #fname, _ = QtGui.QFileDialog.getOpenFileName(self, "Open Code", "/home", "XML Files (*.xml)")
        fname, _ = QtGui.QFileDialog.getOpenFileName()

        f = open(fname, 'r')

        with f:
            print 'OPENING: '+fname
            data = f.read()
            code = 'window.openFromFile(\'' + data + '\')'
            self.frame.evaluateJavaScript(code)

    def saveFileDialog(self):
        #fname, _ = QtGui.QFileDialog.getSaveFileName(self, "Save Code", "/home", "XML File (*.xml)")
        fname, _ = QtGui.QFileDialog.getSaveFileName()
        f = open(fname, 'w')

        with f:
            print 'SAVING: '+fname
            data = self.frame.evaluateJavaScript('window.saveToFile()')
            f.write(data)

    def exit(self):
        self.c.closeApp.emit()

    def setupEditor(self):
        self.editor = Editor()
        self.actionOpen.triggered.connect(self.openFileDialog)
        self.actionSave.triggered.connect(self.saveFileDialog)
        self.actionExit.triggered.connect(self.exit)

    def setupJS(self):
        self.frame = self.web.page().mainFrame()
        self.frame.addToJavaScriptWindowObject('editor', self.editor)
        # self.frame.evaluateJavaScript("updateCode();")

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(865, 679)
        self.layoutMain = QtGui.QWidget(MainWindow)
        self.layoutMain.setObjectName("layoutMain")
        self.gridLayout = QtGui.QGridLayout(self.layoutMain)
        self.gridLayout.setObjectName("gridLayout")
        self.web = QtWebKit.QWebView(self.layoutMain)
        self.web.setObjectName("webView")
        self.web.load(QtCore.QUrl("http://127.0.0.1:"+str(MainWindow.port)+"/html/blockly.html"))
        self.gridLayout.addWidget(self.web, 1, 0, 1, 1)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.comboBox = QtGui.QComboBox(self.layoutMain)
        self.comboBox.setObjectName("comboBox")
        self.horizontalLayout.addWidget(self.comboBox)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.buttonStart = QtGui.QPushButton(self.layoutMain)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonStart.sizePolicy().hasHeightForWidth())
        self.buttonStart.setSizePolicy(sizePolicy)
        self.buttonStart.setCheckable(False)
        self.buttonStart.setAutoExclusive(False)
        self.buttonStart.setObjectName("buttonStart")
        self.horizontalLayout.addWidget(self.buttonStart)
        self.buttonStop = QtGui.QPushButton(self.layoutMain)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonStop.sizePolicy().hasHeightForWidth())
        self.buttonStop.setSizePolicy(sizePolicy)
        self.buttonStop.setCheckable(False)
        self.buttonStop.setAutoExclusive(False)
        self.buttonStop.setDefault(False)
        self.buttonStop.setFlat(False)
        self.buttonStop.setObjectName("buttonStop")
        self.horizontalLayout.addWidget(self.buttonStop)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.layoutMain)
        self.menuBar = QtGui.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 865, 25))
        self.menuBar.setObjectName("menuBar")
        self.menuDevice = QtGui.QMenu(self.menuBar)
        self.menuDevice.setObjectName("menuDevice")
        self.menuAbout = QtGui.QMenu(self.menuBar)
        self.menuAbout.setObjectName("menuAbout")
        MainWindow.setMenuBar(self.menuBar)
        self.actionAbout = QtGui.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        self.actionHelp = QtGui.QAction(MainWindow)
        self.actionHelp.setObjectName("actionHelp")
        self.actionSave = QtGui.QAction(MainWindow)
        self.actionSave.setObjectName("actionSave")
        self.actionSave_As = QtGui.QAction(MainWindow)
        self.actionSave_As.setObjectName("actionSave_As")
        self.actionExit = QtGui.QAction(MainWindow)
        self.actionExit.setObjectName("actionExit")
        self.actionNew = QtGui.QAction(MainWindow)
        self.actionNew.setObjectName("actionNew")
        self.actionOpen = QtGui.QAction(MainWindow)
        self.actionOpen.setObjectName("actionOpen")
        self.menuDevice.addAction(self.actionNew)
        self.menuDevice.addAction(self.actionOpen)
        self.menuDevice.addSeparator()
        self.menuDevice.addAction(self.actionSave)
        self.menuDevice.addAction(self.actionSave_As)
        self.menuDevice.addSeparator()
        self.menuDevice.addAction(self.actionExit)
        self.menuAbout.addAction(self.actionHelp)
        self.menuAbout.addSeparator()
        self.menuAbout.addAction(self.actionAbout)
        self.menuBar.addAction(self.menuDevice.menuAction())
        self.menuBar.addAction(self.menuAbout.menuAction())

        self.buttonStop.setDisabled(True)

        self.retranslateUi(MainWindow)
        QtCore.QObject.connect(self.buttonStart, QtCore.SIGNAL("clicked(bool)"), self.buttonStop.setDisabled)
        QtCore.QObject.connect(self.buttonStart, QtCore.SIGNAL("clicked(bool)"), self.buttonStart.setEnabled)
        QtCore.QObject.connect(self.buttonStop, QtCore.SIGNAL("clicked(bool)"), self.buttonStart.setDisabled)
        QtCore.QObject.connect(self.buttonStop, QtCore.SIGNAL("clicked(bool)"), self.buttonStop.setEnabled)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "GUINO - Editor", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonStart.setText(QtGui.QApplication.translate("MainWindow", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.buttonStop.setText(QtGui.QApplication.translate("MainWindow", "Stop", None, QtGui.QApplication.UnicodeUTF8))
        self.menuDevice.setTitle(QtGui.QApplication.translate("MainWindow", "File", None, QtGui.QApplication.UnicodeUTF8))
        self.menuAbout.setTitle(QtGui.QApplication.translate("MainWindow", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionAbout.setText(QtGui.QApplication.translate("MainWindow", "About", None, QtGui.QApplication.UnicodeUTF8))
        self.actionHelp.setText(QtGui.QApplication.translate("MainWindow", "Help", None, QtGui.QApplication.UnicodeUTF8))
        self.actionSave.setText(QtGui.QApplication.translate("MainWindow", "Save", None, QtGui.QApplication.UnicodeUTF8))
        self.actionExit.setText(QtGui.QApplication.translate("MainWindow", "Exit", None, QtGui.QApplication.UnicodeUTF8))
        self.actionNew.setText(QtGui.QApplication.translate("MainWindow", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOpen.setText(QtGui.QApplication.translate("MainWindow", "Open", None, QtGui.QApplication.UnicodeUTF8))

